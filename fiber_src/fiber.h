#define ALIGNMENT       16

void * ConvertThreadToFiber(void);
void * CreateFiber(int size_stack, void* function, void* args );
void * SwitchToFiber(void*);

long FlsAlloc(void);
int FlsFree(long);
long long FlsGetValue(long);
void FlsSetValue(long, long long);