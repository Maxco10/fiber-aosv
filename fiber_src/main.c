#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include "fiber.h"

#define STACK_SIZE 1024
#define N_FIB      6
#define N_THREAD   3

void *sum(void * args) __attribute__ ((noreturn));
void *stuff(void * arg) __attribute__ ((noreturn));
long i = 0;

pthread_mutex_t lock;

void		**fibers[N_FIB];
pthread_t 	*threads;
long 		store_fls[N_FIB];

struct arg_sum{
	long int id;
	long int a;
};

void *stuff(void * id){
	long int id_tr = (long int) id;
	long int f_id = (N_FIB - N_THREAD) + id_tr;
	fibers[f_id] = ConvertThreadToFiber();
	printf("id = %ld\n",f_id);

	struct arg_sum* arg = (struct arg_sum*) malloc(sizeof(struct arg_sum) * N_FIB);
	arg -> id = f_id;
	arg -> a = id_tr+1;

	sum((void*) arg);
	exit(0);
}

void *sum(void * args){
	struct arg_sum *arg = (struct arg_sum*) args;
	long long ii,j;

	printf("[MAIN] Hello Fiber\t%ld,\tthread = %ld\n",arg->id,arg->a);

	long fls = FlsAlloc();
	store_fls[arg->id] = fls;
	FlsSetValue(fls,(long long) arg);

	for(ii=0;ii<1000;ii++){
		j+=1;
	}

	if(arg->id != 0){
		if(arg->id > 1){
			printf("++[MAIN] fiber %ld get %ld from fls[%ld] = %ld\n", arg->id, ((struct arg_sum *) FlsGetValue(store_fls[arg->id - 1]))->id, arg->id-1, store_fls[arg->id - 1]);
		}
		i+=1;
		SwitchToFiber(fibers[i]);
	}

	getchar();
	exit(0);
}

int main(){
	struct arg_sum** args = (struct arg_sum**) malloc(sizeof(struct arg_sum*) * (N_FIB - N_THREAD));
	threads = (pthread_t*) malloc(sizeof(pthread_t) * N_THREAD);
	long int i, err;

	fibers[0] = ConvertThreadToFiber();

	args[0] 		= (struct arg_sum*) malloc(sizeof(struct arg_sum));
	args[0] -> id 	= 0;
	args[0] -> a  	= 0;
	
	for(i = 1; i < N_FIB - N_THREAD; i++){
		args[i] = (struct arg_sum*) malloc(sizeof(struct arg_sum));
		args[i] -> id = i;
		args[i] -> a  = i;
		fibers[i] = CreateFiber(STACK_SIZE,sum,(void*) args[i]);
	}

	printf("[STEP 0] -> Fibre create e pronte\npremere un tasto per continuare...\n");

	for(i = 0; i<N_THREAD; i++){
		err = pthread_create(&threads[i], NULL, stuff, (void *)i);
		if(err!=0){
			printf("Errore creazione thread\n");
			return 0;
		}
	}

	SwitchToFiber(fibers[1]);
	return 0;
}
