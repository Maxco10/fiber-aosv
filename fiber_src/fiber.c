#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>

#include "fiber.h"
#include "kernel_module/src/ioctl.h"

__thread long int this;

//function to create the first fiber and to create the context fiber.
void* ConvertThreadToFiber(){
	long ret, fd;
	int c;


	//Open the device with read/write access
	fd = open("/dev/fiber", O_RDWR);            
	if (fd < 0){
		perror("[ERROR][Convert] Failed to open the device.");
		exit(EXIT_FAILURE);
	}
	//I use the IOCTL with CONVET_THREAD
	ret = ioctl(fd, (unsigned int) CONVERT_THREAD);
	if (ret < 0){
		perror("[ERROR][Convert] Failed to perform ioctl command.");
		exit(EXIT_FAILURE);
	}

	c = close(fd);
	if(c < 0){
		perror("[ERROR][Convert] Failed close device.");
		exit(EXIT_FAILURE);
	}
	this = ret;
	return (void*) ret;
}
//function to create the fiber
void* CreateFiber(int size_stack, void* function, void* args ){
	long ret, fd;
	int c;
	void* stack_pointer;
	size_t reminder;
	int n_page;
	int page_size = getpagesize();

	struct arg_device* arg 	= (struct arg_device*)malloc(sizeof(struct arg_device));
	if(arg == NULL){
		perror("[ERROR][Create] Failed to alloc the arg.");
		return (void*) &errno;
	}

	//Sanity check
	if (size_stack <= 0) {
		size_stack = SIGSTKSZ;
	}
	//Align the size to the page boundary
	reminder = size_stack % page_size;
	n_page = size_stack / page_size;
	if (reminder != 0) {
		size_stack += (page_size - reminder);
	}

	//Malloc for stack fiber
	c = posix_memalign(&stack_pointer, ALIGNMENT, size_stack);
	if(c != 0){
		if(c == EINVAL) perror("[ERROR][Create] (In posix_memalign) EINVAL: The alignment argument was not a power of two, or was not a multiple of sizeof(void *)");
		else if(c == ENOMEM) perror("[ERROR][Create] (In posix_memalign) ENOMEM: There was insufficient memory to fulfill the allocation request.");
		return (void*) &errno;
	}
	bzero(stack_pointer, size_stack);

	arg-> thread_id 		= 0;		            			//The thread id
	arg-> arg_function 		= args;								//The argorment for the function
	arg-> function 			= function;							//The pointer to function
	arg-> size_stack 		= size_stack;						//The size stack of the fiber
	arg-> stack_pointer		= stack_pointer;					//Pointer to fiber's stack

	//Open the device with read/write access
	fd = open("/dev/fiber", O_RDWR);            
	if (fd < 0){
		perror("[ERROR][Create] Failed to open the device.");
		exit(EXIT_FAILURE);
	}

	//I use the IOCTL with CREATE_FIBER
	ret = ioctl(fd,(unsigned int) CREATE_FIBER,(unsigned long) arg);
	if (ret < 0){
		if(ret == -2)
			perror("[ERROR][Create] You MUST first convert the fiber thread with ConvertThreadToFiber() function.");
		else 
			perror("[ERROR][Create] Failed to perform ioctl command.");

		exit(EXIT_FAILURE);
	}

	free(arg);
	c = close(fd);
	if(c < 0){
		perror("[ERROR][Create] Failed close device.");
		exit(EXIT_FAILURE);
	}
	return (void*) ret;
}
//function to switch two fibers
void* SwitchToFiber( void* fiber_to ){
	long ret, fd;
	int c;
	int copy = this;

	struct arg_device_switch arg = {
		.fiber_id_to 	= (long int) fiber_to,
		.fiber_id_from	= this
	};

	this = (long int) fiber_to;
	
	//Open the device with read/write access
	fd = open("/dev/fiber", O_RDWR);            
	if (fd < 0){
		perror("[ERROR][Switch] Failed to open the device.");
		exit(EXIT_FAILURE);
	}

	//I use the IOCTL with CREATE_FIBER
	ret = ioctl(fd,(unsigned int) SWITCH_FIBER,(unsigned long) &arg);
	if (ret == 0){
		perror("[ERROR][Switch] Failed to perform ioctl command.");
		exit(EXIT_FAILURE);
	}
	if (ret == 2){
		this = copy;
	}

	c = close(fd);
	if(c < 0){
		perror("[ERROR][Switch] Failed close device.");	
		exit(EXIT_FAILURE);	
	}
	//this = (long int) fiber_to;
	return NULL;
}

long FlsAlloc(void){
	long ret, fd;
	int c;

	struct arg_device_fls arg = {
		.fls_index      = -1,	//Index get,set or free fls but now useless here.
		.fiber_id 		= this,
		.value			= (void *)-1,	//Initialize to -1(useless here);
	};

	//Open the device with read/write access
	fd = open("/dev/fiber", O_RDWR);            
	if (fd < 0){
		perror("[ERROR][FLS_ALLOC] Failed to open the device.");
		exit(EXIT_FAILURE);
	}

	//I use the IOCTL with CREATE_FIBER
	ret = ioctl(fd,(unsigned int) FLS_ALLOC,(unsigned long) &arg);
	if(ret < 0){
		perror("[ERROR][FLS_ALLOC] Failed to perform ioctl command.");
		exit(EXIT_FAILURE);
	}

	c = close(fd);
	if(c < 0){
		perror("[ERROR][FLS_ALLOC] Failed close device.");
		exit(EXIT_FAILURE);
	};
	return ret;
}

int FlsFree(long index_free){
	long ret, fd;
	int c;
	struct arg_device_fls arg = {
		.fls_index 		= index_free,   //Index used to free the fls.
		.fiber_id 		= this,
		.value			= (void *)-1,			//Initialize to -1(useless here);
	};

	//Open the device with read/write access
	fd = open("/dev/fiber", O_RDWR);            
	if (fd < 0){
		perror("[ERROR][FLS_FREE] Failed to open the device.");
		exit(EXIT_FAILURE);
	}

	//I use the IOCTL with CREATE_FIBER
	ret = ioctl(fd,(unsigned int) FLS_FREE,(unsigned long) &arg);
	if (ret < 0){
		perror("[ERROR][FLS_FREE] Failed to perform ioctl command.");
		exit(EXIT_FAILURE);
	}

	c = close(fd);
	if(c < 0){
		perror("[ERROR][FLS_FREE] Failed close device.");
		exit(EXIT_FAILURE);
	};
	return 1;
}

long long FlsGetValue(long index){
	long long ret;
	long fd;
	int c;

	struct arg_device_fls arg = {
		.fls_index 		= index,		//Index used to get value.
		.fiber_id 		= this,
		.value			= (void *)-1,	//Initialize to -1(useless here);
	};

	//Open the device with read/write access
	fd = open("/dev/fiber", O_RDWR);            
	if (fd < 0){
		perror("[ERROR][FLS_GET] Failed to open the device.");
		exit(EXIT_FAILURE);
	}

	//I use the IOCTL with FLS_GET
	ret = ioctl(fd,(unsigned int) FLS_GET,(unsigned long) &arg);
	if (ret < 0){
		perror("[ERROR][FLS_GET] Failed to perform ioctl command.");
		exit(EXIT_FAILURE);
	}

	c = close(fd);
	if(c < 0){
		perror("[ERROR][FLS_GET] Failed close device.");
		exit(EXIT_FAILURE);
	};
	return (long long) arg.value;
}

void FlsSetValue(long int index, long long value){
	long ret, fd;
	int c;
	struct arg_device_fls arg = {
		.fls_index 		= index, 	 //Index used to set value.
		.fiber_id 		= this,
		.value			= (void *)value,     //Value;

	};

	//Open the device with read/write access
	fd = open("/dev/fiber", O_RDWR);            
	if (fd < 0){
		perror("[ERROR][FLS_SET] Failed to open the device.");
		exit(EXIT_FAILURE);
	}

	//I use the IOCTL with CREATE_FIBER
	ret = ioctl(fd,(unsigned int) FLS_SET,(unsigned long) &arg);
	if (ret == -1){
		perror("[ERROR][FLS_SET] Failed to perform ioctl command.");
		exit(EXIT_FAILURE);
	}

	c = close(fd);
	if(c < 0){
		perror("[ERROR][FLS_SET] Failed close device.");
		exit(EXIT_FAILURE);
	};
}



