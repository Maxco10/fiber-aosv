/******** IOCTL PART ***************/

#define SCULL_IOC_MAGIC 'H'
/*
IO if we do not need the parameters
IOW if we need some parameters into one structure. The user space writes the parameters for the kernel.
*/

#define CONVERT_THREAD _IO(SCULL_IOC_MAGIC, 1 )
#define CREATE_FIBER   _IOW(SCULL_IOC_MAGIC, 3, struct arg_device)
#define SWITCH_FIBER   _IOW(SCULL_IOC_MAGIC, 4, struct arg_device_switch)

#define FLS_ALLOC      _IOW(SCULL_IOC_MAGIC, 6,    struct arg_device_fls )
#define FLS_FREE       _IOW(SCULL_IOC_MAGIC, 7,    struct arg_device_fls )
#define FLS_GET        _IOW(SCULL_IOC_MAGIC, 8,    struct arg_device_fls )
#define FLS_SET        _IOW(SCULL_IOC_MAGIC, 9,    struct arg_device_fls )

struct arg_device{
	unsigned long     thread_id;       // The thread id
	void*             function;         // The pointer to function
	void*             arg_function;     // The argoment of the function
	void*             stack_pointer;    // Pointer to fiber's stack
	int               size_stack;       // The size stack of the fiber
};

struct arg_device_switch{
	long int fiber_id_to;      // Fiber to run
	long int fiber_id_from;
};

struct arg_device_fls{
	long 		fls_index;     // Index used to free,get and set the fls.
	long int 	fiber_id;      // Fiber id
	void 		*value;        // Fls's value
};