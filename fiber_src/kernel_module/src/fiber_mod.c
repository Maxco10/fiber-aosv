#include <linux/module.h>
#include <linux/device.h>
#include <asm/fpu/internal.h>

#include "device.h"
#include "fiber_function.h"
#include "ioctl.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Antonio & Marco");
MODULE_DESCRIPTION("AOSV Project");
MODULE_VERSION("1.0");

//spinlock
spinlock_t		lock1;
unsigned long 	flag_switch;

spinlock_t		lock2;
unsigned long 	flag_fls;

spinlock_t 		lock3;
unsigned long 	flag_create;

spinlock_t 		lock4;
unsigned long 	flag_fls_alloc;

static struct hlist_head ht_process[MY_HT_SIZE];

//DEBUG FUNCION, print all process information, also all fiber information for each process
void print_all(){
	struct process_t 	*process;
	struct fiber_t 		*fiber;
	struct fls_t 		*fls;
	struct hlist_node 	*tmp1, *tmp2, *tmp3;
	int 				i, j;
	i = 0;
	j = 0;
	printk(KERN_INFO "Fiber_print_all: =============================");
	hash_for_each_safe(ht_process,i,tmp1,process,list){
		printk(KERN_INFO "Process: Process_ID %d,\ttot fibers = %d,\ttot_fls = %ld",process->process_id, atomic_read(&process->numbers_fibers),atomic_long_read(&process->tot_fls));
		hash_for_each_safe(process->fiber_list,j,tmp2,fiber,list){
			printk(KERN_INFO "Fiber:\tFiber_ID %d,\tThread_parent %ld, active: %ld",fiber->fiber_id, fiber->process_id, fiber->active);
		}
		hash_for_each_safe(process->fls_list,j,tmp3,fls,list){
			printk(KERN_INFO "FLS:\tFLS_ID %ld \tValue %lld",atomic_long_read(&fls->fls_id),(long long int)fls->value);
		}
	}
	printk(KERN_INFO "Fiber_print_all: =============================\n");
}

struct process_t* find_process_by_id(long int pid){
	struct process_t* process;
	struct hlist_node *next;
	hash_for_each_possible_safe(ht_process, process, next, list, pid){
		if((long int)process->process_id == pid){
			return process;
		}
	}
	return NULL;
}

struct fiber_t * find_fiber_by_id(long int pid,long int fid){
	struct process_t *process;
	struct fiber_t   *fiber;
	struct hlist_node *next;

	process = find_process_by_id(pid);
	if(process == NULL){
		pr_info("[INFO] Process not found in find_fiber_by_id\n");
		return NULL;
	}
	hash_for_each_possible_safe(process->fiber_list,fiber, next, list, fid){
		if((long int)fiber -> fiber_id == fid){
			return fiber;
		}
	}
	pr_info("[INFO] Fiber not found in find_fiber_by_id\n");
	return NULL;
}

/**
 * The init funcion, initialize all struct and create the "char device" to use fibers. 
 *  @return returns 0 if successful
 */
static int __init fiber_init(void){
	printk(KERN_INFO "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
	printk(KERN_INFO "Fiber: Initializing Fiber module\n");
	spin_lock_init(&lock1);
	spin_lock_init(&lock2);
	spin_lock_init(&lock3);
	spin_lock_init(&lock4);
	hash_init(ht_process);
	// Try to dynamically allocate a major number for the devic
	majorNumber = register_chrdev(0, DEVICE_NAME, &fops);
	if (majorNumber<0){
		printk(KERN_ALERT "Fiber failed to register a major number\n");
		return majorNumber;
	}
	// Register the device class
	fiberClass = class_create(THIS_MODULE, CLASS_NAME);
	if (IS_ERR(fiberClass)){                // Check for error and clean up if there is
		unregister_chrdev(majorNumber, DEVICE_NAME);
		printk(KERN_ALERT "Fiber: Failed to register device class\n");
		return PTR_ERR(fiberClass);          // Correct way to return an error on a pointer
	}
	// Register the device driver
	fiberDevice = device_create(fiberClass, NULL, MKDEV(majorNumber, 0), NULL, DEVICE_NAME);
	if (IS_ERR(fiberDevice)){               // Clean up if there is an error
		class_destroy(fiberClass);           // Repeated code but the alternative is goto statements
		unregister_chrdev(majorNumber, DEVICE_NAME);
		printk(KERN_ALERT "Failed to create the device\n");
		return PTR_ERR(fiberDevice);
	}
	// Register the kprobe
	kprobe_init();
	return 0;
}

// __exit function in invoked when teh module is removed.
static void __exit fiber_exit(void){
	device_destroy(fiberClass, MKDEV(majorNumber, 0));     // remove the device
	class_unregister(fiberClass);                          // unregister the device class
	class_destroy(fiberClass);                             // remove the device class
	unregister_chrdev(majorNumber, DEVICE_NAME);           // unregister the major number
	kprobe_exit();                                         // unregister the kprobe
	printk(KERN_INFO "Fiber: Goodbye from fiber!\n");
}

static long dev_ioctl(struct file *filp, unsigned int cmd, unsigned long arg){
	struct process_t 			*new_process;
	struct fiber_t 				*new_fiber;
	struct fls_t 				*fls_target;
	struct fls_t 				*fls_target_2;
	struct arg_device 			*arg_read;
	struct arg_device_switch 	*arg_read_swtich;
	struct arg_device_fls 		*arg_read_fls;
	struct process_t 			*current_process;
	struct fls_t 				*new_fls;
	struct fiber_t 				*fiber_a;
	struct fiber_t 				*fiber_b;
	struct process_t 			*scan;
	struct hlist_node 			*next;
	struct process_t 			*control;
	unsigned long 				c;

	control		= NULL;
	new_process = NULL;

	switch(cmd){

		case CONVERT_THREAD:

			spin_lock_irqsave(&lock3,flag_create);

			control = find_process_by_id(current->tgid);
			if(control == NULL){
				// Set the data of process
				new_process 					= kmalloc(sizeof(*new_process),GFP_KERNEL);
				new_process -> process_id 		= current->tgid;
				new_process -> list_fiber		= NULL;
				atomic_set(&new_process -> numbers_fibers,1);
				atomic_long_set(&new_process -> tot_fls,0);
				atomic_long_set(&new_process -> n_thread, 1);

				//Set the data of fiber_0
				new_fiber                     = kmalloc(sizeof(*new_fiber),GFP_KERNEL);
				new_fiber -> fiber_id         = 0;
				new_fiber -> entrance_stime   = current->stime;
				new_fiber -> exit_stime       = 0;
				new_fiber -> entrance_utime   = current->utime;
				new_fiber -> exit_utime       = 0;
				new_fiber -> total_execution  = 0;
				new_fiber -> function         = NULL;
				new_fiber -> arg_function     = NULL;
				new_fiber -> thread_id_created        = current->pid;
				new_fiber -> thread_id_running        = current->pid;
				new_fiber -> process_id       = current->tgid;
				new_fiber -> stack_base       = NULL;
				new_fiber -> size_stack       = 0;
				new_fiber -> active           = 1;
				set_bit(0, &new_fiber -> active);

				atomic_long_set(&(new_fiber -> num_activation), 0);
				atomic_long_set(&(new_fiber -> num_activation_fail), 0);

				new_fiber->context         = (struct context_t*) kmalloc(sizeof(struct context_t),GFP_KERNEL);
				new_fiber->context->regs   = (struct pt_regs*) kmalloc(sizeof(struct pt_regs), GFP_KERNEL);
				new_fiber->context->r_fpu   = (struct fpu*) kmalloc(sizeof(struct fpu), GFP_KERNEL);

				//Save register
				memcpy(new_fiber->context->regs, current_pt_regs(), sizeof(struct pt_regs));
				fpu__save(new_fiber->context->r_fpu);

				// Add the process to process_list
				hash_add(ht_process,&(new_process->list),new_process->process_id);

				// Add the fiber to process
				hash_init(new_process->fiber_list);
				hash_add(new_process->fiber_list,&new_fiber->list,new_fiber->fiber_id);

				//Init the FLS
				hash_init(new_process->fls_list);

			} else {
				//Set the data of fiber_0
				new_fiber                     = kmalloc(sizeof(*new_fiber),GFP_KERNEL);
				new_fiber -> fiber_id         = atomic_read(&control -> numbers_fibers);
				new_fiber -> entrance_stime   = current->stime;
				new_fiber -> exit_stime       = 0;
				new_fiber -> entrance_utime   = current->utime;
				new_fiber -> exit_utime       = 0;
				new_fiber -> total_execution  = 0;
				new_fiber -> function         = NULL;
				new_fiber -> arg_function     = NULL;
				new_fiber -> thread_id_created        = current->pid;
				new_fiber -> thread_id_running        = current->pid;
				new_fiber -> process_id       = current->tgid;
				new_fiber -> stack_base       = NULL;
				new_fiber -> size_stack       = 0;
				new_fiber -> active           = 1;
				set_bit(0, &new_fiber -> active);

				atomic_long_set(&(new_fiber -> num_activation), 0);
				atomic_long_set(&(new_fiber -> num_activation_fail), 0);
			 
				new_fiber->context          = (struct context_t*) kmalloc(sizeof(struct context_t),GFP_KERNEL);
				new_fiber->context->regs    = (struct pt_regs*) kmalloc(sizeof(struct pt_regs), GFP_KERNEL);
				memcpy(new_fiber->context->regs,  current_pt_regs(), sizeof(struct pt_regs));

				new_fiber->context->r_fpu   = (struct fpu*) kmalloc(sizeof(struct fpu), GFP_KERNEL);
				fpu__save(new_fiber->context->r_fpu);

				atomic_inc(&control -> numbers_fibers);
				atomic_long_inc(&control -> n_thread);

				// Add the fiber to process
				hash_add(control->fiber_list,&new_fiber->list,new_fiber->fiber_id);
			}
			spin_unlock_irqrestore(&lock3,flag_create);
			return new_fiber -> fiber_id;
		case CREATE_FIBER:

			arg_read = (struct arg_device*) kmalloc(sizeof(struct arg_device),GFP_KERNEL);
			c = copy_from_user(arg_read,(struct arg_device*) arg,sizeof(struct arg_device));
			if(c != 0){
				printk(KERN_ALERT "Fiber: error during copy from user, c = %ld",c);
				return -1;
			}

			spin_lock_irqsave(&lock3,flag_create);
			current_process = NULL;
			hash_for_each_possible_safe(ht_process,scan,next,list,current->tgid){
				if(scan->process_id == current->tgid){
					current_process = scan;
					break;
				}
			}

			if(current_process == NULL){
				printk(KERN_ALERT "Fiber: [ERROR] Process not found in 'create_fiber'");
				spin_unlock_irqrestore(&lock3,flag_create);
				return -2;
			}

			new_fiber 						= kmalloc(sizeof(*new_fiber),GFP_KERNEL);
			new_fiber -> fiber_id 			= atomic_read(&current_process->numbers_fibers);
			new_fiber -> entrance_stime 	= 0;
			new_fiber -> exit_stime 		= 0;
			new_fiber -> entrance_utime 	= 0;
			new_fiber -> exit_utime 		= 0;
			new_fiber -> total_execution 	= 0;
			new_fiber -> function 			= arg_read->function;
			new_fiber -> arg_function 		= arg_read->arg_function;
			new_fiber -> thread_id_created 			= current->pid;
			new_fiber -> thread_id_running        = current->pid;
			new_fiber -> process_id 		= current->tgid;
			new_fiber -> stack_base 		= arg_read->stack_pointer;
			new_fiber -> size_stack 		= arg_read->size_stack;
			new_fiber -> active 			= 0;
			clear_bit(0, &new_fiber -> active);
			atomic_long_set(&new_fiber -> num_activation_fail, 0);
			atomic_long_set(&new_fiber -> num_activation, 0);

			new_fiber->context 			= (struct context_t*) kmalloc(sizeof(struct context_t),GFP_KERNEL);
			new_fiber->context->regs 	= (struct pt_regs*) kmalloc(sizeof(struct pt_regs), GFP_KERNEL);
			new_fiber->context->r_fpu 	= (struct fpu*) kmalloc(sizeof(struct fpu), GFP_KERNEL);

			memcpy(new_fiber->context->regs, current_pt_regs() ,sizeof(struct pt_regs));
			fpu__save(new_fiber->context->r_fpu);

			new_fiber->context->regs->sp = (unsigned long) arg_read  -> stack_pointer + (arg_read->size_stack) - 8;
			new_fiber->context->regs->di = (unsigned long) new_fiber -> arg_function;
			new_fiber->context->regs->ip = (unsigned long) arg_read  -> function;

			atomic_inc(&current_process -> numbers_fibers);

			hash_add(current_process->fiber_list,&new_fiber->list,new_fiber->fiber_id);

			kfree(arg_read);
			spin_unlock_irqrestore(&lock3,flag_create);
			return new_fiber -> fiber_id; 
		case SWITCH_FIBER:

			fiber_a  = NULL;     // Fiber to exit
			fiber_b  = NULL;     // Fiber to entrance

			arg_read_swtich = (struct arg_device_switch*) kmalloc(sizeof(struct arg_device_switch),GFP_KERNEL);
			c = copy_from_user(arg_read_swtich,(struct arg_device_switch*) arg, sizeof(struct arg_device_switch));
			if(c!=0){
				printk(KERN_ALERT "Fiber: error during copy from user, c = %ld",c);
				kfree(arg_read_swtich);
				return 0;
			}

			spin_lock_irqsave(&lock1,flag_switch);
			fiber_a = find_fiber_by_id( current->tgid,  arg_read_swtich->fiber_id_from);
			if(fiber_a == NULL){
				printk(KERN_ALERT "Fiber: [ERROR] Fiber A (ID %ld) not found in switch, process id = %d | thread id = %d\n", arg_read_swtich -> fiber_id_from, current -> tgid, current -> pid);
				kfree(arg_read_swtich);
				spin_unlock_irqrestore(&lock1,flag_switch);
				return 0;
			}

			fiber_b = find_fiber_by_id( current->tgid,  arg_read_swtich->fiber_id_to);
			if(fiber_b == NULL){
				printk(KERN_ALERT "Fiber: [ERROR] Fiber B (ID %ld) not found in switch, process id = %d | thread id = %d\n", arg_read_swtich -> fiber_id_to, current -> tgid, current -> pid);
				kfree(arg_read_swtich);
				spin_unlock_irqrestore(&lock1,flag_switch);
				return 0;
			}
			
			pr_info("Fiber: from %d (act: %d, thr: %ld) -> to %d (act: %d, thr: %ld), process %d\n", 	fiber_a -> fiber_id, 
																										test_bit(0, &(fiber_a -> active)),
																										fiber_a -> thread_id_running,
																										fiber_b -> fiber_id,
																										test_bit(0, &(fiber_b -> active)),
																										fiber_b -> thread_id_running,
																										current->tgid);
			if(test_bit(0, &(fiber_b -> active))){
				atomic_long_inc(&fiber_b -> num_activation_fail);
				pr_info("Fiber: Fibra %d gia attiva! (active = %ld )", fiber_b -> fiber_id ,fiber_b -> active);
				spin_unlock_irqrestore(&lock1,flag_switch);
				kfree(arg_read_swtich);
				return 2;
			}
			//Timer
			fiber_a -> exit_utime = current -> utime;
			fiber_a -> exit_stime = current -> stime;

			fiber_a -> total_execution += (fiber_a -> exit_utime - fiber_a -> entrance_utime);
			fiber_a -> total_execution += (fiber_a -> exit_stime - fiber_a -> entrance_stime);

			fiber_b -> entrance_utime = current -> utime;
			fiber_b -> entrance_stime = current -> stime;

			memcpy(fiber_a->context->regs, current_pt_regs(), sizeof(struct pt_regs));
			fpu__save(fiber_a->context->r_fpu);

			//Copy the context of fiber B into current!
			fpu__restore(fiber_b->context->r_fpu);
			memcpy(current_pt_regs(), fiber_b->context->regs, sizeof(struct pt_regs));

			clear_bit(0, &(fiber_a -> active));
			set_bit(0, &(fiber_b -> active));

			fiber_b -> thread_id_running = current -> pid;

			
			atomic_long_inc(&fiber_b->num_activation);
			spin_unlock_irqrestore(&lock1,flag_switch);
			kfree(arg_read_swtich);
			return 1;
		case FLS_ALLOC:
			// I take the argoments from struct
			arg_read_fls = (struct arg_device_fls*) kmalloc(sizeof(struct arg_device_fls),GFP_KERNEL);
			c = copy_from_user(arg_read_fls,(struct arg_device_fls*) arg, sizeof(struct arg_device_fls));
			if(c!=0){
				printk(KERN_ALERT "Fiber-FLS: error during copy from user, c = %ld",c);
				kfree(arg_read_fls);
				return -1;
			}

			spin_lock_irqsave(&lock4,flag_fls_alloc);

			//I take the current process
			current_process = find_process_by_id(current->tgid); 
			if(current_process == NULL){
				printk(KERN_ALERT "Fiber-FLS: prcess not found!");
				kfree(arg_read_fls);
				spin_unlock_irqrestore(&lock4,flag_fls_alloc);
				return -2;
			}
			// I take the fiber's struct that request the fls
			fiber_a = find_fiber_by_id(current_process->process_id, arg_read_fls->fiber_id);
			if(fiber_a == NULL){
				printk(KERN_ALERT "Fiber-FLS: fiber not found!");
				kfree(arg_read_fls);
				spin_unlock_irqrestore(&lock4,flag_fls_alloc);
				return -3;
			}

			// Set the structure of FLS
			new_fls =  kmalloc(sizeof(*new_fls),GFP_KERNEL);
			atomic_long_set(&new_fls-> fls_id, atomic_long_read(&current_process->tot_fls));
			new_fls->value = (void*) -1;
			// Update the fls id in the fls'structure
			atomic_long_inc(&current_process->tot_fls);
			// Add the fls to hashlist and free the memory
			hash_add(current_process->fls_list,&new_fls->list,atomic_long_read(&new_fls->fls_id));
			kfree(arg_read_fls);
			spin_unlock_irqrestore(&lock4,flag_fls_alloc);
			return atomic_long_read(&new_fls-> fls_id);
		case FLS_FREE:
			// I take the argoments from struct
			arg_read_fls = (struct arg_device_fls*) kmalloc(sizeof(struct arg_device_fls),GFP_KERNEL);
			c = copy_from_user(arg_read_fls,(struct arg_device_fls*) arg, sizeof(struct arg_device_fls));
			if(c!=0){
				printk(KERN_ALERT "Fiber-FLS: error during copy from user, c = %ld",c);
				kfree(arg_read_fls);
				return -1;
			}
			//I take the current process
			current_process = find_process_by_id(current->tgid);
			if(current_process == NULL){
				printk(KERN_ALERT "Fiber-FLS: prcess not found!");
				kfree(arg_read_fls);
				return -1;
				}
			// I searche the fls and remove it
			hash_for_each_possible_safe(current_process->fls_list,fls_target, next, list,arg_read_fls->fls_index){
				if(atomic_long_read(&fls_target->fls_id) == arg_read_fls->fls_index)
					hash_del(&fls_target->list);
			}
			// Reset the key in the fiber 
			fls_target->value = (void*) -1;
			kfree(arg_read_fls);
			return 0;
		case FLS_GET:
			//I take the argoments from struct
			arg_read_fls = (struct arg_device_fls*) kmalloc(sizeof(struct arg_device_fls),GFP_KERNEL);
			c = copy_from_user(arg_read_fls,(struct arg_device_fls*) arg, sizeof(struct arg_device_fls));
			if(c!=0){
				printk(KERN_ALERT "Fiber-FLS: error during copy from user, c = %ld",c);
				kfree(arg_read_fls);
				return 0;
			}
			//I take the current process
			current_process = find_process_by_id(current->tgid);
			if(current_process == NULL){
				printk(KERN_ALERT "Fiber-FLS: prcess not found!");
				kfree(arg_read_fls);
				return 0;
			}
			// I search the value in the fls and return it
			hash_for_each_possible_safe(current_process->fls_list,fls_target, next, list,arg_read_fls->fls_index){
				if(atomic_long_read(&fls_target->fls_id) == arg_read_fls->fls_index){
					arg_read_fls -> value = fls_target->value;
					copy_to_user((void *)arg,arg_read_fls,sizeof(struct arg_device_fls));
					kfree(arg_read_fls);
					return 0;
				}
			}
			kfree(arg_read_fls);       
			// I return this result if i did not find the fls entry.
			return 0;
		case FLS_SET:

			spin_lock_irqsave(&lock2,flag_fls);
			// I take the argoments from struct
			arg_read_fls = (struct arg_device_fls*) kmalloc(sizeof(struct arg_device_fls),GFP_KERNEL);
			c = copy_from_user(arg_read_fls,(struct arg_device_fls*) arg, sizeof(struct arg_device_fls));
			if(c!=0){
				printk(KERN_ALERT "Fiber-FLS: error during copy from user, c = %ld",c);
				spin_unlock_irqrestore(&lock2,flag_fls);
				kfree(arg_read_fls);
				return -1;
			}
			//I take the current process
			current_process = find_process_by_id(current->tgid);
			if(current_process == NULL){
				printk(KERN_ALERT "Fiber-FLS: prcess not found!");
				spin_unlock_irqrestore(&lock2,flag_fls);
				kfree(arg_read_fls);
				return -1;
			}
			// I search the value in the fls and set it
			hash_for_each_possible_safe(current_process->fls_list, fls_target_2, next, list, arg_read_fls->fls_index){
				if(atomic_long_read(&fls_target_2->fls_id) == arg_read_fls->fls_index){
					fls_target_2->value = arg_read_fls->value;
					spin_unlock_irqrestore(&lock2,flag_fls);
					kfree(arg_read_fls);
					return 0;
				}
			}
			//I return this result if i did not find the fls entry.
			spin_unlock_irqrestore(&lock2,flag_fls);
			kfree(arg_read_fls);
			return 0;
		default:
			break;
	}
	return 1;
}

module_init(fiber_init);
module_exit(fiber_exit);
