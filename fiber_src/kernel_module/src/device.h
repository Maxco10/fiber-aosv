//Devise Info
#define  DEVICE_NAME "fiber"    // The device will appear at /dev/fiber using this value
#define  CLASS_NAME  "fibers"   // The device class -- this is a character device driver

// The prototype functions for the character driver -- must come before the struct definition
static long    dev_ioctl(struct file *filp, unsigned int cmd, unsigned long arg);

static int    			majorNumber;                 // Stores the device number -- determined automatically
static struct class*  	fiberClass  = NULL;  // The device-driver class struct pointer
static struct device* 	fiberDevice = NULL;  // The device-driver device struct pointer

//operation supported by char device
static struct file_operations fops = {
	.unlocked_ioctl = dev_ioctl,  // Handler of IOCTL calls
};
