#include "fiber_function.h"

//Struct for fiber file into proc.
struct file_operations fiber_proc_fops = {
	.open 		= fiber_single_open,
	.read 		= seq_read,
	.llseek 	= generic_file_llseek,
};

//Readdir kprobe
struct kprobe kp_1 = {
	.symbol_name   = symbol_1,
};
//Lookup kprobe
struct kprobe kp_2 = {
	.symbol_name   = symbol_2,
};
//do_exit kprobe
struct kprobe kp_3 = {
	.symbol_name   = symbol_3,
};

//Fiber's directory properties
struct pid_entry_c add_dir = {
	.name = "fiber",
	.len  = sizeof("fiber")-1,
	.mode = S_IFDIR|S_IRUGO|S_IXUGO,
	.fop  = NULL,
	.iop  = NULL,
	.op   = {},
};

struct pid_entry_c         *list_directories;
struct pid_entry_c         *list_directories_with_fiber; //copy of list directories with new folder "fiber"

int fiber_single_show(struct seq_file *m, void *v){
	struct inode *inode = m->private;
	struct pid_namespace *ns;
	struct pid *pid;
	struct task_struct *task;
	int ret;
	ns = inode->i_sb->s_fs_info;
	pid = container_of(inode, struct proc_inode, vfs_inode)->pid;
	task = get_pid_task(pid, PIDTYPE_PID);
	if (!task)
		return -ESRCH;
	ret = container_of(inode, struct proc_inode, vfs_inode)->op.proc_show(m, ns, pid, task);
	put_task_struct(task);
	return ret;
}

int fiber_single_open(struct inode *inode, struct file *filp){
	return single_open(filp, fiber_single_show, inode);
}
//Function to print the informations
int f_show(struct seq_file *m, struct pid_namespace *ns, struct pid *pid, struct task_struct *task){

	const struct file    *file_fiber;         //File
	long                 id_fiber;            //Fiber's id target
	long                 id_process;          //Process' id target
	struct process_t*    process_target;      //Struct of target process
	struct fiber_t*      fiber_target;        //Struct of target fiber

	file_fiber = m->file;                     //I get the file target
	kstrtol(file_fiber->f_path.dentry->d_name.name,10,&id_fiber);  //I get the fiber id from path file and converts it to long
	kstrtol(file_fiber->f_path.dentry->d_parent->d_parent->d_name.name,10,&id_process); //I get the process id from path file and converts it to long
	process_target = find_process_by_id((long int)id_process);                    //I recover the struct of target process
	fiber_target = find_fiber_by_id(id_process,id_fiber);       //I recover the struct of target fiber

	//I prepare the informations
	seq_printf(m,"##### Struct Fiber #####\n");
	seq_printf(m,"Fiber ID:\t\t\t %d\n",fiber_target->fiber_id);
	seq_printf(m,"Process ID:\t\t\t %ld\n",fiber_target->process_id);
	seq_printf(m,"Thread created ID:\t\t\t %ld\n",fiber_target->thread_id_created);
	seq_printf(m,"Thread running ID:\t\t\t %ld\n",fiber_target->thread_id_running);
	seq_printf(m,"Active ID:\t\t\t %ld\n",fiber_target->active);
	seq_printf(m,"Number of activations:\t\t %ld\n",atomic_long_read(&fiber_target->num_activation));
	seq_printf(m,"Number of activations failed:\t %ld\n",atomic_long_read(&fiber_target->num_activation_fail));
	seq_printf(m,"Total time execution:\t\t %lld milliseconds\n",(fiber_target->total_execution)/1000000);

	return 0;
}

//Function that call when I mount the module.
int kprobe_init(){
	int ret;
	
	list_directories 			= NULL;
	list_directories_with_fiber = NULL;
	//Set the handler kprobe for readdir(1) and lookup(2)
	kp_1.pre_handler 	= handler_pre_readdir;
	kp_1.fault_handler 	= handler_fault;

	kp_2.pre_handler 	= handler_pre_lookup;
	kp_2.fault_handler 	= handler_fault;

	kp_3.pre_handler 	= handler_pre_do_exit;
	kp_3.fault_handler 	= handler_fault;


	//I register the kprobe
	ret = register_kprobe(&kp_1);
	if (ret < 0) {
		pr_err("register_kprobe_1 failed, returned %d\n", ret);
		return ret;
	}
	pr_info("Planted kprobe at %p\n", kp_1.addr);
	ret = register_kprobe(&kp_2);
	if (ret < 0) {
		pr_err("register_kprobe_2 failed, returned %d\n", ret);
		return ret;
	}
	pr_info("Planted kprobe at %p\n", kp_2.addr);

	ret = register_kprobe(&kp_3);
	if (ret < 0) {
		pr_err("register_kprobe_3 failed, returned %d\n", ret);
		return ret;
	}
	pr_info("Planted kprobe at %p\n", kp_3.addr);

	return 0;
}
//Function that call when I remove the module.
void kprobe_exit(void){
	//Remove the kprobe from system and free the memory
	unregister_kprobe(&kp_1);
	unregister_kprobe(&kp_2);
	unregister_kprobe(&kp_3);
	pr_info("kprobe at %p unregistered\n", kp_1.addr);
	pr_info("kprobe at %p unregistered\n", kp_2.addr);
	pr_info("kprobe at %p unregistered\n", kp_3.addr);
	kfree(list_directories_with_fiber);
}

//Function to faults into kprobe
int handler_fault(struct kprobe *p, struct pt_regs *regs, int trapnr){
	pr_info("fault_handler: p->addr = 0x%p, trap #%dn", p->addr, trapnr);
	return 0;
}

//Functio to patch proc readdir
int handler_pre_readdir(struct kprobe *p, struct pt_regs *regs){
	struct file 		*file;
	struct dentry 		*parent;             //struct parent
	unsigned long 		size;
	long 				parent_long;         //pid parent
	long 				current_pid;         //current pid
	int 				i;
	int 				ret;
	int 				number_fibers;       //number of fibers of target process
	char 				*name_fiber;         //variable for name of fiber files.
	struct process_t 	*process_target;      //struct of the calling process
	struct pid_entry_c	*list_fiber; 

	size 	= (unsigned long) regs-> cx;           //Size of directory
	file 	= (void*)         regs-> di;           //File properties
	parent 	= file->f_path.dentry->d_parent;       //name of process
	process_target = NULL;
	list_fiber = NULL;

	if(strcmp(file->f_path.dentry->d_name.name,"fiber") == 0 ) {		//I am here, if the fiber folder is present and the user called it.
		//Convert the pid process to long and I recover the structure of process, because I need number of fibers.
		ret = kstrtol(parent->d_name.name,10,&parent_long);
		if(ret != 0){
			pr_warn("Fiber_error: error in kstrtol of handler_pre_readdir (check parent pid)");
			return 0;
		}
		process_target = find_process_by_id((long int)parent_long);

		if(process_target != NULL){
			number_fibers = atomic_read(&process_target -> numbers_fibers);
			//Allocate memory list fibers for the number of fibers I have.
			if(process_target -> list_fiber == NULL){
				list_fiber = (struct pid_entry_c*) kmalloc(sizeof(struct pid_entry_c)*(number_fibers),GFP_KERNEL);
			} else {
				kfree(process_target -> list_fiber);
				kfree(list_fiber);
				list_fiber = (struct pid_entry_c*) kmalloc(sizeof(struct pid_entry_c)*(number_fibers),GFP_KERNEL);
			}

			//Allocate the memory for the name of fiber files of MAX_CHAR(as into standard proc 256).
			name_fiber = (char*) kmalloc(sizeof(char)*MAX_CHAR,GFP_KERNEL);

			//I prepare the file structure for each fiber.
			for(i = 0; i < number_fibers; i++){

				//I convert the index to string and copy into name.
				sprintf(name_fiber,"%d", i);
				list_fiber[i].name = (char*) kmalloc(sizeof(char)*MAX_CHAR,GFP_KERNEL);
				memcpy(list_fiber[i].name, name_fiber, strlen(name_fiber)+1);

				list_fiber[i].len 			= strlen(name_fiber);		//Len name file
				list_fiber[i].mode 			= S_IFREG|S_IRUGO|S_IWUGO;	//Mode of file(regular file read)
				list_fiber[i].fop 			= &fiber_proc_fops;			//Pointer to file operation
				list_fiber[i].iop    		= NULL;						//Pointer to inode operation
				list_fiber[i].op.proc_show 	= f_show;					//I set the function for proc show.
			}

			process_target -> list_fiber = list_fiber;
			//free the memory
			kfree(name_fiber);

			//I update the list file with my new list(i.e. fiber's file) and its size.
			regs -> dx = (unsigned long) process_target -> list_fiber;
			regs -> cx = (unsigned long) number_fibers;
		}
	} else {
		ret = kstrtol(file->f_path.dentry->d_name.name,10,&current_pid);
		if(ret != 0 ){
			pr_info("Fiber_error: error '%d' in kstrtol of handler_pre_readdir (check pid)",ret);
			return 0;
		}

		process_target = find_process_by_id((long int)current_pid);
		if(process_target != NULL){
			size  = (unsigned long) regs-> cx;	//I recover the directory's size.
			if(list_directories == NULL && list_directories_with_fiber == NULL){
				//I prepare a my copy of array with size+1. +1 because I must add my /fiber directory
				list_directories_with_fiber  = (struct pid_entry_c*) kmalloc(sizeof(struct pid_entry_c)*(size+1) ,GFP_KERNEL);
				list_directories             = (struct pid_entry_c*) regs-> dx;

				//For setting the permissions, I copy the permission of general directory, I chose "attr" directory.
				//Because it should be the first in the array and therefore I should not spend much time researching.
				//I find the folder, I stop the loop and copy its attributes and set them on my structure for the fiber folder.
				for(i = 0; i < size; i++){
					if(strcmp(list_directories[i].name ,"attr") == 0){
						add_dir.fop = list_directories[i].fop;
						add_dir.iop = list_directories[i].iop;
						break;
					}
				}

				//I copy all old array into mine and I add / fiber to the empty position.
				memcpy(list_directories_with_fiber, list_directories ,sizeof(struct pid_entry_c)*size);
				memcpy(&list_directories_with_fiber[size],&add_dir,sizeof(struct pid_entry_c));
			}
			//I update the register with my new values.
			regs -> dx = (unsigned long) list_directories_with_fiber;
			regs -> cx = (unsigned long) size + 1;
		}
	}
	return 0;
}


//Functio to patch proc lookup
int handler_pre_lookup(struct kprobe *p, struct pt_regs *regs){

	struct dentry 		*dentry;
	struct dentry 		*parent;             //struct parent
	unsigned long 		size;
	long 				parent_long;         //pid parent
	long 				current_pid;         //current pid
	int 				i;
	int 				ret;
	int 				number_fibers;       //number of fibers of target process
	char 				*name_fiber;         //variable for name of fiber files.
	struct process_t 	*process_target;      //struct of the calling process
	struct pid_entry_c	*list_fiber; 

	size 	= (unsigned long) regs-> cx; 	//Size of directory
	dentry 	= (void*)         regs-> di; 	//File properties
	parent 	= dentry->d_parent;       		//name of process
	process_target = NULL;
	list_fiber = NULL;

	if(strcmp(dentry->d_name.name,"fiber") == 0 ) {		//I am here, if the fiber folder is present and the user called it.
		//Convert the pid process to long and I recover the structure of process, because I need number of fibers.
		ret = kstrtol(parent->d_name.name,10,&parent_long);
		if(ret != 0){
			pr_warn("Fiber_error: error in kstrtol of handler_pre_readdir (check parent pid)");
			return 0;
		}
		process_target = find_process_by_id((long int)parent_long);

		if(process_target != NULL){
			number_fibers = atomic_read(&process_target -> numbers_fibers);
			//Allocate memory list fibers for the number of fibers I have.
			if(process_target -> list_fiber == NULL){
				list_fiber = (struct pid_entry_c*) kmalloc(sizeof(struct pid_entry_c)*(number_fibers),GFP_KERNEL);
			} else {
				kfree(process_target -> list_fiber);
				kfree(list_fiber);
				list_fiber = (struct pid_entry_c*) kmalloc(sizeof(struct pid_entry_c)*(number_fibers),GFP_KERNEL);
			}

			//Allocate the memory for the name of fiber files of MAX_CHAR(as into standard proc 256).
			name_fiber = (char*) kmalloc(sizeof(char)*MAX_CHAR,GFP_KERNEL);

			//I prepare the file structure for each fiber.
			for(i = 0; i < number_fibers; i++){

				//I convert the index to string and copy into name.
				sprintf(name_fiber,"%d", i);
				list_fiber[i].name = (char*) kmalloc(sizeof(char)*MAX_CHAR,GFP_KERNEL);
				memcpy(list_fiber[i].name, name_fiber, strlen(name_fiber)+1);

				list_fiber[i].len 			= strlen(name_fiber);   //Len name file
				list_fiber[i].mode 			= (S_IFREG|S_IRUGO);    //Mode of file(regular file read)
				list_fiber[i].fop 			= &fiber_proc_fops;     //Pointer to file operation
				list_fiber[i].iop    		= NULL;                 //Pointer to inode operation
				list_fiber[i].op.proc_show 	= f_show;         //I set the function for proc show.
			}

			process_target -> list_fiber = list_fiber;
			//free the memory
			kfree(name_fiber);

			//I update the list file with my new list(i.e. fiber's file) and its size.
			regs -> dx = (unsigned long) process_target -> list_fiber;
			regs -> cx = (unsigned long) number_fibers;
		}
	} else {
		ret = kstrtol(dentry->d_name.name,10,&current_pid);
		if(ret != 0 ){
			//pr_info("Fiber_error: error '%d' in kstrtol of handler_pre_readdir (check pid)",ret);
			return 0;
		}

		process_target = find_process_by_id((long int)current_pid);
		if(process_target != NULL){
			size  = (unsigned long) regs-> cx;	//I recover the directory's size.
			if(list_directories == NULL && list_directories_with_fiber == NULL){
				//I prepare a my copy of array with size+1. +1 because I must add my /fiber directory
				list_directories_with_fiber  = (struct pid_entry_c*) kmalloc(sizeof(struct pid_entry_c)*(size+1) ,GFP_KERNEL);
				list_directories             = (struct pid_entry_c*) regs-> dx;

				//For setting the permissions, I copy the permission of general directory, I chose "attr" directory.
				//Because it should be the first in the array and therefore I should not spend much time researching.
				//I find the folder, I stop the loop and copy its attributes and set them on my structure for the fiber folder.
				for(i = 0; i < size; i++){
					if(strcmp(list_directories[i].name ,"attr") == 0){
						add_dir.fop = list_directories[i].fop;
						add_dir.iop = list_directories[i].iop;
						break;
					}
				}

				//I copy all old array into mine and I add / fiber to the empty position.
				memcpy(list_directories_with_fiber, list_directories ,sizeof(struct pid_entry_c)*size);
				memcpy(&list_directories_with_fiber[size],&add_dir,sizeof(struct pid_entry_c));
			}

			//I update the register with my new values.
			regs -> dx = (unsigned long) list_directories_with_fiber;
			regs -> cx = (unsigned long) size + 1;
		}
	}
	return 0;
}

//Functio to patch do_exit
int handler_pre_do_exit(struct kprobe *p, struct pt_regs *regs){
	struct process_t           *current_process;
	struct fls_t               *fls_target;  
	struct fiber_t             *fiber_a;
	int                         i;

	current_process = NULL;
	i = 0;
	current_process = find_process_by_id((long int)current->tgid);
	if(current_process != NULL){
		if(atomic_long_read(&current_process -> n_thread) == 1){
			//print_all();
			hash_for_each(current_process->fiber_list,i,fiber_a,list){
				hash_del(&fiber_a->list);
				kfree(fiber_a->context->regs);
				kfree(fiber_a->context->r_fpu);
				kfree(fiber_a->context);
				kfree(fiber_a);
			}
			//Delete fls entry in the current process
			hash_for_each(current_process->fls_list,i,fls_target,list){
				hash_del(&fls_target->list);
			}
			hash_del(&current_process->list);
			kfree(current_process -> list_fiber);
			kfree(current_process);
			printk(KERN_INFO "Fiber: Delete all fibers of %d",current_process->process_id);
		} else {
			atomic_long_dec(&current_process -> n_thread);
		}
	}
	return 0;
}