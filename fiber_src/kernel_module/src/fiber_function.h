#include <linux/hashtable.h>
#include <linux/kprobes.h>
#include <linux/seq_file.h>
#include <linux/slab.h>

#define MY_HT_SIZE      2000

// The struct that store all the fiber information
struct fiber_t {
	void                    *stack_base;
	void                    *function;
	void                    *arg_function;
	u64                     total_execution;
	u64                     entrance_utime;
	u64                     exit_utime;
	u64                     entrance_stime;
	u64                     exit_stime;
	atomic64_t              num_activation_fail;
	atomic64_t              num_activation;
	volatile unsigned long  active;
	unsigned long           thread_id_created;
   unsigned long           thread_id_running;
	long                    process_id;
	int                     size_stack;
	int                     fiber_id;
	struct hlist_node       list;
	struct context_t        *context; //MUST last position
};

// The struct that mainten all usefull information of the proecess
struct process_t {
	struct pid_entry_c		*list_fiber;              //List of fiber files into /fiber
	atomic64_t				tot_fls;
	atomic64_t				n_thread;
	atomic_t 				numbers_fibers;           // Total number of the fibers of process.
	int 					process_id;
	struct hlist_head 		fiber_list[MY_HT_SIZE];   // The list of fibers of process.
	struct hlist_head 		fls_list[MY_HT_SIZE];     // The list of fls
	struct hlist_node 		list;                     // List of process (hashtable)
};

// The struct that mainten all usefull information of the FLS
struct fls_t {
	atomic64_t 			fls_id;              // FLS id
	void 				*value;        
	struct hlist_node 	list;
};

struct context_t {
	struct pt_regs *regs;
	struct fpu     *r_fpu;
};

extern struct process_t* find_process_by_id(long int pid);
extern struct fiber_t* find_fiber_by_id(long int pid, long int fid);
extern void print_all(void);

//==========================================================
/********** PROC FUNCTION *****************/

#define MAX_SYMBOL_LEN	64       // Mac char for function patch
#define MAX_CHAR		256      // Max char for fiber file name

//names of the function that I must patch.
#define symbol_1	"proc_pident_readdir"
#define symbol_2 	"proc_pident_lookup"
#define symbol_3	"do_exit"

// Proc function for fiber
int fiber_single_show(struct seq_file *m, void *v);
int fiber_single_open(struct inode *inode, struct file *filp);

union proc_op_c {
	int (*proc_get_link)(struct dentry *, struct path *);
	int (*proc_show)(struct seq_file *m, struct pid_namespace *ns, struct pid *pid,  struct task_struct *task);
};

struct pid_entry_c {
	char                       *name;
	unsigned int               len;
	umode_t                    mode;
	struct inode_operations    *iop;
	struct file_operations     *fop;
	union proc_op_c            op;
};

struct proc_inode {
	struct pid                       *pid;
	unsigned int                     fd;
	union proc_op_c                  op;
	struct proc_dir_entry            *pde;
	struct ctl_table_header          *sysctl;
	struct ctl_table                 *sysctl_entry;
	struct hlist_node                sysctl_inodes;
	const struct proc_ns_operations  *ns_ops;
	struct inode                     vfs_inode;
} __randomize_layout;

// Function to recover the fiber info
int f_show(struct seq_file *m, struct pid_namespace *ns, struct pid *pid, struct task_struct *task);

//Function for (un)planted and management of krprobe 
int  handler_pre_readdir(struct kprobe *p, struct pt_regs *regs);
int  handler_pre_lookup(struct kprobe *p, struct pt_regs *regs);
int  handler_pre_do_exit(struct kprobe *p, struct pt_regs *regs);
//void handler_post(struct kprobe *p, struct pt_regs *regs, unsigned long flags);
int  handler_fault(struct kprobe *p, struct pt_regs *regs, int trapnr);

extern int  kprobe_init(void);
extern void kprobe_exit(void);