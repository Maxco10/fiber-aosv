# Advanced Operating Systems and Virtualization - A.A. 2017/2018

<u>Professor</u>: Alessandro Pellegrini

<u>Students</u>:  Antonio Montini 1532918 - Marco Magnani 1532000

## Introduzione

Una fibra è un’unità di esecuzione che deve essere schedulata manualmente dall'applicazione. Le fibre sono eseguite su dei thread. Ogni thread può eseguire qualsiasi fibra del processo. Le fibre simulano una situazione in cui un *Kernel Level Thread* ha abbinato più di un *User Level Thread*.   

Ogni fibra può richiedere un fiber local storage (*FLS*), un’area di memoria condivisa fra tutte le fibre dello stesso processo.

Le la creazione e utilizzo delle fibre la libreria offre se seguenti 3 funzioni:

- *ConvertThreadToFiber*: che deve essere chiamata prima di qualsiasi altra funzione di *fiber* perché essa è responsabile di inizializzare e allocare tutte le risorse necessarie per le fibre. Tale funzione deve essere chiamata da tutti i thread utilizzati.
- *CreateFiber*: questa funzione è responsabile di inizializzare le fibre create con i parametri passati dall’utente.
- *SwitchFiber*: questa funzione è responsabile di effettuare il context switch tra la fibra chiamante e la fibra di destinazione.

Per poter utilizzare le *FLS* abbiamo:

- *FLS_ALLOC*: alloca la memoria e ritorna l’identificativo della FLS allocata.
- *FLS_FREE*: libera la *FLS* passata come parametro.
- *FLS_GET*: restituisce il contenuto presente nella *FLS* passata come parametro.
- *FLS_SET*: Imposta il valore fornito come parametro nella *FLS* bersaglio.

In qualsiasi momento dell'esecuzione è possibile vedere lo “*stato*” di ogni fibra del processo in *procfs*,  all’interno della cartella */proc/<tgid>/fiber* del processo in run.

Nella presente implementazione abbiamo utilizzato diverse componenti:

- In `USER SPACE` forniamo una libreria che espone tutte le funzioni necessarie al funzionamento del sistema.
- In `KERNEL SPACE`  è presente un modulo che è responsabile della gestione delle fibre. Si occupa della gestione di tutte le strutture dati necessarie a mantenere le informazioni sui processi, sulle fibre, sulle FLS e della creazione dei file in *procfs*.
-  Per eseguire le le interazioni  `USER <-> KERNEL SPACE ` abbiamo utilizzato un CHAR DEVICE che fa uso di IOCTL per eseguire le funzioni e passare i dati da un ambiente all'altro.

## Libreria utente

Come abbiamo detto nell’introduzione abbiamo implementato un libreria utente, *fiber.h*. Essa è un interfaccia che permette all'utente finale di utilizzare le fibre in modo completamente trasparente. Di seguito la lista completa delle funzioni:

- `void* convertThreadToFiber()` : Converte il thread che la invoca in una fibra e inizializza tutte le strutture necessarie per l'utilizzarle, sia a livello utente che a livello di kernel. Tutti i thread che dovranno gestire le fibre devono chiamare prima questa funzione.

- `void* createFiber(int size_stack, void* function, void* args)`: Crea un fibra pronta per essere eseguita, i parametri di input sono:
  - *int size_stack* : un numero intero che specifica la grandezza dello stack che la fibra dovrà utilizzare.
  - *void* * *funcion* : un puntatore alla funzione che sarà eseguita dalla fibra quando andrà in esecuzione.
  - *void* * *args*: Un puntatore agli argomenti della funzione data in input.
- `void* SwitchToFiber( void* fiber_to )`: Effettua lo switch tra due fibre. Il valore di input è l'ID della fibra bersaglio. Cioè, il valore di ritorno della funzione `createFiber`  o `convertThreadToFiber`.
- `long FlsAlloc(void)` : Alloca una FLS per la fibra chiamante e ritorna l'indice di tale FLS.
- `int FlsFree(long index_free)` : Libera le risorse allocate per la fibra bersaglio, identificata dal paramento passato come input. 
- `long long FlsGetValue(long index)`: Ritorna il valore della FLS bersaglio,  identificata dal paramento passato come input. 
- `void FlsSetValue(long int index, long long value)`: Assegna il valore `value` alla FLS identificata da `index`

Ogni funzione presenta controlli di errore che nel caso falliscano le chiamate al modulo kernel terminano il processo con il flag `EXIT_FAILURE`.

In questo file vengono utilizzate, diverse strutture che permettono il passaggio di valori tra spazio utente e spazio kernel:

- `arg_device`: utilizzata nel createFiber
- `arg_device_swith`: utilizzata nella funzione switchToFiber.
- `arg_device_fls`: utilizzata in tutte le funzioni di *FLS* ma con i campi riempiti in diverso modo in base alla funzione che la sta utilizzando.

Le strutture in comune tra spazio kernel e spazio utente sono presenti nel file *ioctl.h*.

## Strutture Kernel

In questa implementazione le strutture kernel giocano un ruolo chiave. Procedendo per linea gerarchica, abbiamo utilizzato un *hash map* con una dimensione di 2000 bucket per la gestione dei processi che hanno richiesto di lavorare con Fiber.

La struttura  è  di tipo`process_t` ed è presente nell'HashMap globale `ht_process` . Tale struttura contiene alcuni campi chiave per il funzionamento del sistema:

|        Tipo         | Nome variabile |                         Descrizione                          |
| :-----------------: | :------------: | :----------------------------------------------------------: |
|  struct hlist_head  |   fiber_list   |               Lista delle fibre del processo.                |
|  struct hlist_head  |    fls_list    |                 Lista delle fls del processo                 |
|         int         |   process_id   |             Identificativo univoco del processo              |
|      atomic_t       | number_fibers  | Numero di fibre create dal processo, campo fondamentale per la numerazione delle fibre. |
|     atomic64_t      |    tot_fls     | Numero di fls richieste dal processo, campo fondamentale per numerazione delle fls. |
|     atomic64_t      |    n_thread    |                Numero di thread del processo                 |
| struct pid_entry_c* |   list_fiber   |        Lista di file fiber dentro la cartella /fiber.        |

La seconda struttura che andiamo a vedere è, *fiber_t*. Essa è la struttura per singola fibra. Lo stato della fibre è rispecchiato in essa. I campi della *fiber_t* sono:

|       Tipo       |   Nome variabile    |                         Descrizione                          |
| :--------------: | :-----------------: | :----------------------------------------------------------: |
|       void       |    *stack_ base     |                Puntare alla base della stack.                |
|       void       |      *function      |             Puntatore alla funzione della fibra              |
|       void       |    *arg_function    | Puntatore alla struttura dei parametri in caso la funzione da eseguire ne abbia bisogno. |
|       u64        |   total_execution   |           Tempo totale di esecuzione di una fibra.           |
|    atomic64_t    | num_activation_fail |          Numero di attivazioni fallite della fibra.          |
|    atomic64_t    |   num_activation    |              Numero di attivazioni della fibra.              |
|  unsigned long   |       active        |         Indica se la fibra è attiva(1) oppure no(0)          |
|  unsigned long   |  thread_id_create   | Identificativo del thread che ha la chiamato la creazione della fibra. |
|  unsigned long   |  thread_id_running  |   Identificativo del thread che ha sta eseguendo la fibra.   |
|       long       |     process_id      |                 Identificativo del processo                  |
|       int        |     size_stack      |                    Dimensione della stack                    |
|       int        |      fiber_id       |                  Identificativo della firba                  |
| struct context_t |      *context       | Struttura utilizzata per il context switch, essa contiene il salvataggio del floating point e dei general purpose. |

La struttura citata *context_t*, è utilizzata per il salvataggio del contesto, essa è formata da:

|      Tipo      | Nome variabile |               Descrizione               |
| :------------: | :------------: | :-------------------------------------: |
| struct pt_regs |     *regs      |   Contiene i riferimenti ai registri    |
|   struct fpu   |     *r_fpu     | Contiene il riferimento ai registri FPU |

La struttura che si occupa di mantenere le informazioni per le FLS è *fls_t*. E’ composta da:

|    Tipo    | Nome variabile |        Descrizione         |
| :--------: | :------------: | :------------------------: |
| atomic64_t |     fls_id     | Identificativo della fls.  |
|    void    |     *value     | valore contenuto nella fls |



## Implementazione nel Kernel

In questa implementazione il vero cuore del progetto risiede nel modulo kernel, f*iber_mode.c*. Esso contiene tutte le funzioni per poter lavorare.

Nel modulo, sono implementate la funzioni base per far funzionare Fiber, le funzioni per effettuare le patch con *kprobe* per *procfs*, *do_exit* a alcune funzioni ausiliare.

Partendo dalle funzioni ausiliari:

- `Print_all()`, stampa l’intero contenuto della *ht_process*
- `Find_process_by_id(int pid)`, che preso come input un id del processo restituisce la sua *process_t*. 
- `Find_fiber_by_id(int pid, int fid)`, che presi in input un id del processo e un id della fibra, restituisce la *fiber_t* richiesta.

Quando il modulo viene montato, viene chiamata la funzioni *fiber_init*. Essa prepara il modulo per il lavorare, "pianta" le *kprobe* attraverso la funzione *kprobe_init*, inizializza gli *spinlock* e cosa molto importante configura il *char device*. Come ausilio al *char device* abbiamo creato un file per impostare i suoi permessi, esso va a posizionarsi */etc/udev/rules.d* denominato *99-fiber.rules*.

Il cuore del modulo risiede in *dev_ictl()*, essa la funzione che permette l’utilizzo delle funzioni, *convertThreadToFiber*, *createFiber*, *switchFiber* e tutte le funzioni per FLS.

Nella *convertThreadToFiber*, vengono inizializzate le strutture e la creazione della fibra 0 questo per il primo thread, nel caso il processo chiami più thread viene fatto un controllo e non viene ripetuta la inizializzazione ma ben si la creazione di una fibra. Per evitare la presenza di più thread in questa sezione di codice e quindi avere problemi di concorrenza, abbiamo utilizzato uno *spinlock*.

Nella *createFiber*, viene inizializzata la struttura per la singola fibra configurando tutti i campi presenti. Anche qui è presente un *spinlock* per evitare problemi di concorrenza.

Nelle funzioni sopracitate, viene ritornato id della fibra creata.

Nella *switchToFiber*, avviene il cambio di contesto. E’ presente uno *spinlock* e un sistema di RCU per evitare problemi di concorrenza. Vengono prese la strutture delle fibre tramite id: della fibra di destinazione e della fibra uscente. Vengono calcolati i tempi di esecuzione tramite una sottrazione tra “entrata-uscita” e vengono impostati i contesti in modo da mandare in esecuzione le fibra prescelta e di salvare il contesto per la fibra uscente. 

Nelle funzioni di FLS, esse si limitano a leggere e scrivere dalla struttura corrispondete, ad eccezione di *FLS_ALLOC* che deve allocare delle FLS e restituire l’identificativo e FLS_FREE che deve rilasciare la FLS bersaglio. Anche per le FLS è presente uno *spinlock* per evitare problemi di concorrenza. 

Altra feature molto importante è poter controllare lo stato della singola fibra.

Come gia abbiamo detto, ogni fibra possiede uno "stato", è possibile vederlo all’interno del file sytemfs proc. All’interno della cartella dei <u>soli</u> processi fiber, viene aggiunta una directory “*Fiber*” e al suo interno tanti file quante sono le fibre di tale processo. Questi file sono numerati da 0 a n, dove n è il numero delle fibre. Per poter vedere il contenuto bisogna utilizzare un commando interrogazione (*es. cat)* da terminale seguito dal numero della fibra e verranno stampante a video le informazioni contenute in *fiber_t* per la fibra invocata.

Per effettuare questa feature, abbiamo dovuto effettuare delle patch con l’utilizzo di *kprobe* alle funzioni *readdir* e *lookup*. Alla prima invocazione di una delle due funzioni, viene inserita la cartella “Fiber” per poi copiare i *file operations* e *inode operation* dalla cartella “*attr*”. Tale operazione è unica e non viene più ripetuta. Alle successive invocazioni delle 2 funzioni verranno eseguiti i campi di codice relativi all’inserimento dei file Fiber.

Rimanendo sempre in tema di kprobe, un'altra funzione che abbiamo effettuato una patch è stata la *do_exit*. Essa viene invocata alla fine dell’esecuzione del processo e ha il compito di deallocare la memoria, in modo da evitare di lasciare aree di memoria allocate e non più utilizzabili.

Le feature sopramenzionate relative a procfs e alla *do_exit* è possibile trovarle nel file *fiber_function.c*

Infine ma non per importanza, quando il modulo viene smontato, viene chiamata la funzione *exit* del modulo,la quale si occupa di deallocare la memoria,rimuovere le kprove piantate e smontare il modulo. 

## Compilazione

Abbiamo scritto, un Makefile per semplificare alcune operazioni, le opzioni sono:

- `make`: compilazione di tutti i file Fiber
- `make ins`:
  - Creazine del file rules
  - Spostamento del file rules
  - Caricamento del modulo
- `make rem`:
  - Smontaggio del modulo
  - Rimozione del file rules
- `make clean`: rimozione di tutti i file eseguibili creati in fase di compilazione

## Testcase and Benchmark

In questa sezione andremo a valutare le performance di sistema in  esecuzione. Per fare ciò abbiamo usato come confronto un programma che simula le fibre solo che utilizza i thread di sistema.

I test sono stati effettuati su 3 diverse piattaforme, 2 piattaforme fisiche e 1 virtuale. Su ognuna di esse abbiamo testato il sistema con 5 - 10 - 30 - 50 - 100 - 256 - 512 fibre. I risultati dettagliati sono presenti nel file di test ([TEST](<https://gitlab.com/Maxco10/fiber-aosv/blob/master/Test.xlsx>)).

Qui di seguito possiamo vedere un anteprima immediata dei risultati:

- Hardware Fisico
  - ![HW-Antonio](./ImmaginiTest/HW-Antonio.png)
  - ![HW-Marco](./ImmaginiTest/HW-Marco.png)
- Hardware Virtuale
  - ![VM](./ImmaginiTest/VirtualMachine.png)

Da questi risulati possiamo vedere che lavorare con questa implementazione di fiber è molto dispondioso in termini di tempo. &Egrave; possibile migliorare questa implementazione lavorando sui seguenti punti:
 - Eliminazione dei cicli per la gestione delle fibre lato kernel, sostituendoli con un'implementazione il cui tempo di accesso alla memeria 
è in O(1).
 - Riduzione delle sezioni critiche.



